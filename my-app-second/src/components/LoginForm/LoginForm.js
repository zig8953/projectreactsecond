import React from 'react';
import styles from './LoginForm.scss';
import { Control } from '../Control/index';
import axios from 'axios';

export class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.user = {
            login: '',
            password: ''
        }
    }
    handleInputChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setUser({
            [name]: value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const response = fetch('/api/user', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ user: this.user })
        })

        //��� ������������ ��������� CORS -c����� ����� ����� �����, ���� �������� ����  
        //axios.post('http://localhost:3080/api/', this.user )
        //        .then(res => {
        //            console.log(res);
        //            console.log(res.data);
        //        })
    }

    render() {
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <Control
                        inputType="input"
                        type="text"
                        name="login"
                        labelText="login"
                        onChange={this.handleInputChange}
                        required
                    />

                    <Control
                        inputType="input"
                        name="password"
                        labelText="password"
                        onChange={this.handleInputChange}
                        required
                    />

                     <button className="submitBtn">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        Come
                    </button>
                </form>
            </div>
        );
    }
};
