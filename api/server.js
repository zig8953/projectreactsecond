const express = require('express');
const path = require('path');
const app = express(),
      bodyParser = require("body-parser");
      port = 3080;


app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../my-app-second/build')));


app.post('/api/user', (req, res) => {
  const user = req.body.user;
  console.log('user authorized:::::', user);
  res.json("user authorized");
});

app.get('/', (req,res) => {
  res.send(`<h1>API Running on the port ${port}</h1>`);
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});